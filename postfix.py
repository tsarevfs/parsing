#!/usr/bin/python

class ParseError(RuntimeError):
    pass

class Token:
    def __init__(self, kind, value):
        self.kind = kind
        self.value = value

    def __repr__(self):
        return '%s(\"%s\")' % (self.kind , str(self.value))

class Node:
    def __init__(self, value):
        self.value = value
        self.children = []

    def __repr__(self):
        return str(self.value) + ' [' + ','.join([str(child) for child in self.children]) + ']'  

    def add_child(self, child):
        if child:
            self.children.append(child)


class Parser:
    def __init__(self, expr):
        self._expr = expr
        self._tokenize()

    def _tokenize(self):
        self._tokens = []
        cur_token = ''
        for c in self._expr:
            if not c.isspace():
                cur_token += c
            else:
                if cur_token.isdigit():
                    self._tokens.append(Token('number', int(cur_token)))
                    cur_token = ''
                elif cur_token in ('+', '-', '*'):
                    self._tokens.append(Token('operator', cur_token))
                    cur_token = ''
                else:
                    raise ParseError("Can't deduce token from " + cur_token)


    def parse(self):
        self._tokens_queue = list(reversed(self._tokens))
        return self._parse_S()

    def _parse_S(self):
        if len(self._tokens_queue) == 0:
            raise ParseError('Cant parse empty sequence as S')
        d = self._tokens_queue.pop()
        if d.kind != 'number':
            raise ParseError('Unexpected token: ' + str(d) + ", when parsing S")

        node = Node('S')
        node.add_child(d)
        node.add_child(self._parse_R1())

        return node

    def _parse_R1(self):
        if len(self._tokens_queue) == 0:
            return None

        node = Node('R1')
        node.add_child(self._parse_R2())
        node.add_child(self._parse_R4())
        
        if len(self._tokens_queue) == 0:
                    raise ParseError('Unexpecting end of tokens when parsing R1')

        o = self._tokens_queue.pop()
        if o.kind != 'operator':
            raise ParseError('Unexpected token: ' + str(o) + ", when parsing R1")

        node.add_child(o)

        return node



    def _parse_R2(self):
        if len(self._tokens_queue) == 0:
            return None

        node = Node('R2')
        node.add_child(self._parse_R3())

        return node

    def _parse_R3(self):
        if len(self._tokens_queue) == 0:
            raise ParseError('Cant parse empty sequence as R3')

        node = Node('R3')
        node.add_child(self._parse_R4())

        o = self._tokens_queue.pop()
        if o.kind != 'operator':
            raise ParseError('Unexpected token: ' + str(o) + ", when parsing R3")

        node.add_child(o)
        node.add_child(self._parse_R2())

        return node

    def _parse_R4(self):
        if len(self._tokens_queue) == 0:
            raise ParseError('Cant parse empty sequence as R4')
        d = self._tokens_queue.pop()
        if d.kind != 'number':
            raise ParseError('Unexpected token: ' + str(d) + ", when parsing R4")

        node = Node('R4')
        node.add_child(d)
        node.add_child(self._parse_R2())

        return node

def main():
    p = Parser('2 2 +\n')
    print p.parse()

if __name__ == '__main__':
    main()