from collections import defaultdict as ddict

rules = {
	'E1' : [('E',)],
	'E'  : [('E', 'PLUS', 'T'), ('T',)],
	'T'  : [('T', 'MUL', 'F'), ('F',)],
	'F'  : [('LPAR', 'E', 'RPAR'), ('id',)],
}

rules = {
	'E' : [('T', 'E1')],
	'E1' : [('PLUS', 'T', 'E1'), ('$eps',)],
	'T' : [('F', 'T1')],
	'T1' : [('MUL', 'F', 'T1'), ('$eps',)],
	'F' : [('LPAR', 'E', 'RPAR'), ('id',)],
}

terms = {'id', 'LPAR', 'RPAR', 'PLUS', 'MUL'}

start = 'E1'

start = 'E'
terms = {'id', 'LPAR', 'RPAR', 'PLUS', 'MUL', '$eps'}


symbols = set()
for left, rights in rules.items():
	symbols.add(left)
	for right in rights:
		for symbol in right:
			symbols.add(symbol)


I = {(start, rules[start][0], 0)}

def is_term(elem):
	return elem in terms

def item2str(item):
	left, right, pos = item
	return str(left) + ' -> ' + ' '.join([symbol for symbol in right[:pos]]) + '.' + ' '.join([symbol for symbol in right[pos:]])

def closure(I):
	J = set(I)

	chainges = True
	while chainges:
		chainges = False
		for left, right, pos in J:
			if len(right) > pos and not is_term(right[pos]):
				new_left = right[pos]
				possible_new = [(new_left, new_right, 0) for new_right in rules[new_left]]
				for item in possible_new:
					if not item in J:
						J.add(item)
						chainges = True
						break
				if chainges:
					break

	return J

def goto(I, symbol):
	J = set()
	for left, right, pos in I:
		if len(right) > pos and right[pos] == symbol:
			J.add((left, right, pos + 1))

	return closure(J)

def items(S1):
	fsm = ddict(list)

	next_id = 0
	C = {tuple(closure(S1)) : next_id}
	next_id += 1
	chainges = True
	while chainges:
		chainges = False
		for I, curr_id in C.items():
			for symbol in symbols:
				possible_new = tuple(goto(I, symbol))
				if not possible_new in C and not len(possible_new) == 0:
					C[possible_new] = next_id

					fsm[curr_id].append((next_id, symbol))

					next_id += 1
					chainges = True
					break
			if chainges:
				break
	return C, fsm


def first():
	FIRST = ddict(set)
	for term in terms:
		FIRST[term].add(term)

	for unterm in rules:
		if [] in rules[unterm]:
			FIRST[unterm].add('$eps')

	chainges = True
	first_len = sum([len(FIRST[elem]) for elem in FIRST])
	while chainges:
		for unterm in rules:
			for rule in rules[unterm]:
				produce_eps = True
				for Y in rule:
					FIRST[unterm] |= (FIRST[Y] - {'$eps'})
					if not '$eps' in FIRST[Y]:
						produce_eps = False
						break
				if produce_eps:
					FIRST[unterm].add('$eps')

		new_len = sum([len(FIRST[elem]) for elem in FIRST])
		chainges = first_len != new_len
		first_len = new_len

	return FIRST

first_table = first()


def follow():
	FOLLOW = ddict(set)
	FOLLOW[start] = {'$'}

	chainges = True
	while chainges:
		chainges = False

		for left in rules:
			print 'left: ' + str(left)

			for right in rules[left]:
				tail_first = set()
				for pos, symbol in enumerate(reversed(right)):
					print 'ptf: ' + str(pos) + ' ' + str(tail_first)

					if pos == 0 or '$eps' in tail_first:
						old_len = len(FOLLOW[symbol])
						FOLLOW[symbol] = FOLLOW[symbol].union(FOLLOW[left])

						if len(FOLLOW[symbol]) > old_len:
							print 'CH3: %s : %s' % (symbol, str(FOLLOW[symbol]))
							chainges = True

					to_add = filter(lambda s: s != '$eps', tail_first)
					old_len = len(FOLLOW[symbol])
					FOLLOW[symbol] = FOLLOW[symbol].union(to_add)

					if len(FOLLOW[symbol]) > old_len:
						print 'CH2: %s : %s' % (symbol, str(FOLLOW[symbol])) 
						chainges = True
					symbols_first = set(first_table[symbol])
					if '$eps' in symbols_first and pos != 0:
						tail_first = (symbols_first - {'$eps'}).union(tail_first)
					else:
						tail_first = symbols_first

	return FOLLOW



item_sets, fsm = items(I)
set_strs = []
for item_set, set_id in item_sets.items():
	set_strs.append(str(set_id) + '\n' + '\n'.join([item2str(item) for item in item_set]))
print '\n===================\n'.join([set_str for set_str in set_strs])

print '###################'
state_strs = []
for start_id, ways in fsm.items():
	state_strs.append(str(start_id) + '\n' + '\n'.join([str(way) for way in ways]))
print '\n===================\n'.join([state_str for state_str in state_strs])

print first()
print follow()
