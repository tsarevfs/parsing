%class-name Scanner
%interactive

%%
[[:space:]]+
-?[[:digit:]]                return Parser::INT;
-?[[:digit:]]*\.[[:digit:]]+ return Parser::FLOAT;

"function"                   return Parser::FUNC;
"procedure"                  return Parser::PROC;
"var"                        return Parser::VAR;
"begin"                      return Parser::BEGIN;
"end"                        return Parser::END;
"if"                         return Parser::IF;
"then"                       return Parser::THEN;
"else"                       return Parser::ELSE;
"and"                        return Parser::AND;
"or"                         return Parser::OR;

[[:alpha:]][[:alnum:]_]*     return Parser::ID;

"+"                          return Parser::PLUS;
"-"                          return Parser::MINUS;
"*"                          return Parser::MUL;
"/"                          return Parser::DIV;
"("                          return Parser::LPAR;
")"                          return Parser::RPAR;

":="                         return Parser::ASSIGN;
"="                          return Parser::EQ;
"<>"                         return Parser::NEQ;

":"                          return Parser::COLON;
";"                          return Parser::SEMICOL;
","                          return Parser::COMMA;
"."                          return Parser::DOT;
