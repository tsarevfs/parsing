// Generated by Bisonc++ V4.01.00 on Wed, 15 May 2013 13:07:37 +0400

// $insert class.ih
#include "Parser.ih"

// The FIRST element of SR arrays shown below uses `d_type', defining the
// state's type, and `d_lastIdx' containing the last element's index. If
// d_lastIdx contains the REQ_TOKEN bitflag (see below) then the state needs
// a token: if in this state d_token__ is _UNDETERMINED_, nextToken() will be
// called

// The LAST element of SR arrays uses `d_token' containing the last retrieved
// token to speed up the (linear) seach.  Except for the first element of SR
// arrays, the field `d_action' is used to determine what to do next. If
// positive, it represents the next state (used with SHIFT); if zero, it
// indicates `ACCEPT', if negative, -d_action represents the number of the
// rule to reduce to.

// `lookup()' tries to find d_token__ in the current SR array. If it fails, and
// there is no default reduction UNEXPECTED_TOKEN__ is thrown, which is then
// caught by the error-recovery function.

// The error-recovery function will pop elements off the stack until a state
// having bit flag ERR_ITEM is found. This state has a transition on _error_
// which is applied. In this _error_ state, while the current token is not a
// proper continuation, new tokens are obtained by nextToken(). If such a
// token is found, error recovery is successful and the token is
// handled according to the error state's SR table and parsing continues.
// During error recovery semantic actions are ignored.

// A state flagged with the DEF_RED flag will perform a default
// reduction if no other continuations are available for the current token.

// The ACCEPT STATE never shows a default reduction: when it is reached the
// parser returns ACCEPT(). During the grammar
// analysis phase a default reduction may have been defined, but it is
// removed during the state-definition phase.

// So:
//      s_x[] = 
//      {
//                  [_field_1_]         [_field_2_]
//
// First element:   {state-type,        idx of last element},
// Other elements:  {required token,    action to perform},
//                                      ( < 0: reduce, 
//                                          0: ACCEPT,
//                                        > 0: next state)
// Last element:    {set to d_token__,    action to perform}
//      }

// When the --thread-safe option is specified, all static data are defined as
// const. If --thread-safe is not provided, the state-tables are not defined
// as const, since the lookup() function below will modify them


namespace // anonymous
{
    char const author[] = "Frank B. Brokken (f.b.brokken@rug.nl)";

    enum 
    {
        STACK_EXPANSION = 5     // size to expand the state-stack with when
                                // full
    };

    enum ReservedTokens
    {
        PARSE_ACCEPT     = 0,   // `ACCEPT' TRANSITION
        _UNDETERMINED_   = -2,
        _EOF_            = -1,
        _error_          = 256
    };
    enum StateType       // modify statetype/data.cc when this enum changes
    {
        NORMAL,
        ERR_ITEM,
        REQ_TOKEN,
        ERR_REQ,    // ERR_ITEM | REQ_TOKEN
        DEF_RED,    // state having default reduction
        ERR_DEF,    // ERR_ITEM | DEF_RED
        REQ_DEF,    // REQ_TOKEN | DEF_RED
        ERR_REQ_DEF // ERR_ITEM | REQ_TOKEN | DEF_RED
    };    
    struct PI__     // Production Info
    {
        size_t d_nonTerm; // identification number of this production's
                            // non-terminal 
        size_t d_size;    // number of elements in this production 
    };

    struct SR__     // Shift Reduce info, see its description above
    {
        union
        {
            int _field_1_;      // initializer, allowing initializations 
                                // of the SR s_[] arrays
            int d_type;
            int d_token;
        };
        union
        {
            int _field_2_;

            int d_lastIdx;          // if negative, the state uses SHIFT
            int d_action;           // may be negative (reduce), 
                                    // postive (shift), or 0 (accept)
            size_t d_errorState;    // used with Error states
        };
    };

    // $insert staticdata
    
// Productions Info Records:
PI__ const s_productionInfo[] = 
{
     {0, 0}, // not used: reduction values are negative
     {284, 2}, // 1: program ->  var_sections body
     {285, 0}, // 2: var_sections ->  <empty>
     {285, 2}, // 3: var_sections ->  var_section var_sections
     {287, 2}, // 4: var_section (VAR) ->  VAR declarations
     {288, 0}, // 5: declarations ->  <empty>
     {288, 2}, // 6: declarations ->  declaration declarations
     {289, 4}, // 7: declaration (COLON) ->  identifiers COLON identifier SEMICOL
     {290, 1}, // 8: identifiers ->  identifier
     {290, 3}, // 9: identifiers (COMMA) ->  identifier COMMA identifiers
     {291, 1}, // 10: identifier (ID) ->  ID
     {286, 4}, // 11: body (BEGIN) ->  BEGIN blocks END DOT
     {292, 0}, // 12: blocks ->  <empty>
     {292, 2}, // 13: blocks ->  block blocks
     {293, 3}, // 14: block (BEGIN) ->  BEGIN blocks END
     {293, 4}, // 15: block (ASSIGN) ->  identifier ASSIGN expr SEMICOL
     {293, 2}, // 16: block (SEMICOL) ->  func_call SEMICOL
     {293, 1}, // 17: block ->  if_block
     {296, 4}, // 18: if_block (IF) ->  IF expr THEN block
     {296, 6}, // 19: if_block (IF) ->  IF expr THEN block ELSE block
     {294, 1}, // 20: expr ->  ar_expr
     {294, 3}, // 21: expr (EQ) ->  ar_expr EQ ar_expr
     {294, 3}, // 22: expr (NEQ) ->  ar_expr NEQ ar_expr
     {297, 3}, // 23: ar_expr (PLUS) ->  summand PLUS summand
     {297, 3}, // 24: ar_expr (MINUS) ->  summand MINUS summand
     {297, 1}, // 25: ar_expr ->  summand
     {298, 3}, // 26: summand (MUL) ->  factor MUL factor
     {298, 3}, // 27: summand (DIV) ->  factor DIV factor
     {298, 1}, // 28: summand ->  factor
     {299, 2}, // 29: factor (MINUS) ->  MINUS atom
     {299, 1}, // 30: factor ->  atom
     {300, 1}, // 31: atom (INT) ->  INT
     {300, 1}, // 32: atom (FLOAT) ->  FLOAT
     {300, 1}, // 33: atom ->  func_call
     {300, 1}, // 34: atom ->  identifier
     {300, 3}, // 35: atom (LPAR) ->  LPAR expr RPAR
     {295, 3}, // 36: func_call (LPAR) ->  identifier LPAR RPAR
     {295, 4}, // 37: func_call (LPAR) ->  identifier LPAR arglist RPAR
     {301, 1}, // 38: arglist ->  expr
     {301, 3}, // 39: arglist (COMMA) ->  expr COMMA arglist
     {302, 1}, // 40: program_$ ->  program
};

// State info and SR__ transitions for each state.


SR__ s_0[] =
{
    { { REQ_DEF}, {  5} },                
    { {     284}, {  1} }, // program     
    { {     285}, {  2} }, // var_sections
    { {     287}, {  3} }, // var_section 
    { {     271}, {  4} }, // VAR         
    { {       0}, { -2} },                
};

SR__ s_1[] =
{
    { { REQ_TOKEN}, {            2} }, 
    { {     _EOF_}, { PARSE_ACCEPT} }, 
    { {         0}, {            0} }, 
};

SR__ s_2[] =
{
    { { REQ_TOKEN}, { 3} },         
    { {       286}, { 5} }, // body 
    { {       272}, { 6} }, // BEGIN
    { {         0}, { 0} },         
};

SR__ s_3[] =
{
    { { REQ_DEF}, {  4} },                
    { {     285}, {  7} }, // var_sections
    { {     287}, {  3} }, // var_section 
    { {     271}, {  4} }, // VAR         
    { {       0}, { -2} },                
};

SR__ s_4[] =
{
    { { REQ_DEF}, {  6} },                
    { {     288}, {  8} }, // declarations
    { {     289}, {  9} }, // declaration 
    { {     290}, { 10} }, // identifiers 
    { {     291}, { 11} }, // identifier  
    { {     270}, { 12} }, // ID          
    { {       0}, { -5} },                
};

SR__ s_5[] =
{
    { { DEF_RED}, {  1} }, 
    { {       0}, { -1} }, 
};

SR__ s_6[] =
{
    { { REQ_DEF}, {   9} },              
    { {     292}, {  13} }, // blocks    
    { {     293}, {  14} }, // block     
    { {     272}, {  15} }, // BEGIN     
    { {     291}, {  16} }, // identifier
    { {     295}, {  17} }, // func_call 
    { {     296}, {  18} }, // if_block  
    { {     270}, {  12} }, // ID        
    { {     274}, {  19} }, // IF        
    { {       0}, { -12} },              
};

SR__ s_7[] =
{
    { { DEF_RED}, {  1} }, 
    { {       0}, { -3} }, 
};

SR__ s_8[] =
{
    { { DEF_RED}, {  1} }, 
    { {       0}, { -4} }, 
};

SR__ s_9[] =
{
    { { REQ_DEF}, {  6} },                
    { {     288}, { 20} }, // declarations
    { {     289}, {  9} }, // declaration 
    { {     290}, { 10} }, // identifiers 
    { {     291}, { 11} }, // identifier  
    { {     270}, { 12} }, // ID          
    { {       0}, { -5} },                
};

SR__ s_10[] =
{
    { { REQ_TOKEN}, {  2} },         
    { {       281}, { 21} }, // COLON
    { {         0}, {  0} },         
};

SR__ s_11[] =
{
    { { REQ_DEF}, {  2} },         
    { {     280}, { 22} }, // COMMA
    { {       0}, { -8} },         
};

SR__ s_12[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -10} }, 
};

SR__ s_13[] =
{
    { { REQ_TOKEN}, {  2} },       
    { {       273}, { 23} }, // END
    { {         0}, {  0} },       
};

SR__ s_14[] =
{
    { { REQ_DEF}, {   9} },              
    { {     292}, {  24} }, // blocks    
    { {     293}, {  14} }, // block     
    { {     272}, {  15} }, // BEGIN     
    { {     291}, {  16} }, // identifier
    { {     295}, {  17} }, // func_call 
    { {     296}, {  18} }, // if_block  
    { {     270}, {  12} }, // ID        
    { {     274}, {  19} }, // IF        
    { {       0}, { -12} },              
};

SR__ s_15[] =
{
    { { REQ_DEF}, {   9} },              
    { {     292}, {  25} }, // blocks    
    { {     293}, {  14} }, // block     
    { {     272}, {  15} }, // BEGIN     
    { {     291}, {  16} }, // identifier
    { {     295}, {  17} }, // func_call 
    { {     296}, {  18} }, // if_block  
    { {     270}, {  12} }, // ID        
    { {     274}, {  19} }, // IF        
    { {       0}, { -12} },              
};

SR__ s_16[] =
{
    { { REQ_TOKEN}, {  3} },          
    { {       267}, { 26} }, // ASSIGN
    { {       278}, { 27} }, // LPAR  
    { {         0}, {  0} },          
};

SR__ s_17[] =
{
    { { REQ_TOKEN}, {  2} },           
    { {       282}, { 28} }, // SEMICOL
    { {         0}, {  0} },           
};

SR__ s_18[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -17} }, 
};

SR__ s_19[] =
{
    { { REQ_TOKEN}, { 13} },              
    { {       294}, { 29} }, // expr      
    { {       297}, { 30} }, // ar_expr   
    { {       298}, { 31} }, // summand   
    { {       299}, { 32} }, // factor    
    { {       264}, { 33} }, // MINUS     
    { {       300}, { 34} }, // atom      
    { {       257}, { 35} }, // INT       
    { {       258}, { 36} }, // FLOAT     
    { {       295}, { 37} }, // func_call 
    { {       291}, { 38} }, // identifier
    { {       278}, { 39} }, // LPAR      
    { {       270}, { 12} }, // ID        
    { {         0}, {  0} },              
};

SR__ s_20[] =
{
    { { DEF_RED}, {  1} }, 
    { {       0}, { -6} }, 
};

SR__ s_21[] =
{
    { { REQ_TOKEN}, {  3} },              
    { {       291}, { 40} }, // identifier
    { {       270}, { 12} }, // ID        
    { {         0}, {  0} },              
};

SR__ s_22[] =
{
    { { REQ_TOKEN}, {  4} },               
    { {       290}, { 41} }, // identifiers
    { {       291}, { 11} }, // identifier 
    { {       270}, { 12} }, // ID         
    { {         0}, {  0} },               
};

SR__ s_23[] =
{
    { { REQ_TOKEN}, {  2} },       
    { {       283}, { 42} }, // DOT
    { {         0}, {  0} },       
};

SR__ s_24[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -13} }, 
};

SR__ s_25[] =
{
    { { REQ_TOKEN}, {  2} },       
    { {       273}, { 43} }, // END
    { {         0}, {  0} },       
};

SR__ s_26[] =
{
    { { REQ_TOKEN}, { 13} },              
    { {       294}, { 44} }, // expr      
    { {       297}, { 30} }, // ar_expr   
    { {       298}, { 31} }, // summand   
    { {       299}, { 32} }, // factor    
    { {       264}, { 33} }, // MINUS     
    { {       300}, { 34} }, // atom      
    { {       257}, { 35} }, // INT       
    { {       258}, { 36} }, // FLOAT     
    { {       295}, { 37} }, // func_call 
    { {       291}, { 38} }, // identifier
    { {       278}, { 39} }, // LPAR      
    { {       270}, { 12} }, // ID        
    { {         0}, {  0} },              
};

SR__ s_27[] =
{
    { { REQ_TOKEN}, { 15} },              
    { {       279}, { 45} }, // RPAR      
    { {       301}, { 46} }, // arglist   
    { {       294}, { 47} }, // expr      
    { {       297}, { 30} }, // ar_expr   
    { {       298}, { 31} }, // summand   
    { {       299}, { 32} }, // factor    
    { {       264}, { 33} }, // MINUS     
    { {       300}, { 34} }, // atom      
    { {       257}, { 35} }, // INT       
    { {       258}, { 36} }, // FLOAT     
    { {       295}, { 37} }, // func_call 
    { {       291}, { 38} }, // identifier
    { {       278}, { 39} }, // LPAR      
    { {       270}, { 12} }, // ID        
    { {         0}, {  0} },              
};

SR__ s_28[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -16} }, 
};

SR__ s_29[] =
{
    { { REQ_TOKEN}, {  2} },        
    { {       275}, { 48} }, // THEN
    { {         0}, {  0} },        
};

SR__ s_30[] =
{
    { { REQ_DEF}, {   3} },       
    { {     268}, {  49} }, // EQ 
    { {     269}, {  50} }, // NEQ
    { {       0}, { -20} },       
};

SR__ s_31[] =
{
    { { REQ_DEF}, {   3} },         
    { {     263}, {  51} }, // PLUS 
    { {     264}, {  52} }, // MINUS
    { {       0}, { -25} },         
};

SR__ s_32[] =
{
    { { REQ_DEF}, {   3} },       
    { {     265}, {  53} }, // MUL
    { {     266}, {  54} }, // DIV
    { {       0}, { -28} },       
};

SR__ s_33[] =
{
    { { REQ_TOKEN}, {  8} },              
    { {       300}, { 55} }, // atom      
    { {       257}, { 35} }, // INT       
    { {       258}, { 36} }, // FLOAT     
    { {       295}, { 37} }, // func_call 
    { {       291}, { 38} }, // identifier
    { {       278}, { 39} }, // LPAR      
    { {       270}, { 12} }, // ID        
    { {         0}, {  0} },              
};

SR__ s_34[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -30} }, 
};

SR__ s_35[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -31} }, 
};

SR__ s_36[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -32} }, 
};

SR__ s_37[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -33} }, 
};

SR__ s_38[] =
{
    { { REQ_DEF}, {   2} },        
    { {     278}, {  27} }, // LPAR
    { {       0}, { -34} },        
};

SR__ s_39[] =
{
    { { REQ_TOKEN}, { 13} },              
    { {       294}, { 56} }, // expr      
    { {       297}, { 30} }, // ar_expr   
    { {       298}, { 31} }, // summand   
    { {       299}, { 32} }, // factor    
    { {       264}, { 33} }, // MINUS     
    { {       300}, { 34} }, // atom      
    { {       257}, { 35} }, // INT       
    { {       258}, { 36} }, // FLOAT     
    { {       295}, { 37} }, // func_call 
    { {       291}, { 38} }, // identifier
    { {       278}, { 39} }, // LPAR      
    { {       270}, { 12} }, // ID        
    { {         0}, {  0} },              
};

SR__ s_40[] =
{
    { { REQ_TOKEN}, {  2} },           
    { {       282}, { 57} }, // SEMICOL
    { {         0}, {  0} },           
};

SR__ s_41[] =
{
    { { DEF_RED}, {  1} }, 
    { {       0}, { -9} }, 
};

SR__ s_42[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -11} }, 
};

SR__ s_43[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -14} }, 
};

SR__ s_44[] =
{
    { { REQ_TOKEN}, {  2} },           
    { {       282}, { 58} }, // SEMICOL
    { {         0}, {  0} },           
};

SR__ s_45[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -36} }, 
};

SR__ s_46[] =
{
    { { REQ_TOKEN}, {  2} },        
    { {       279}, { 59} }, // RPAR
    { {         0}, {  0} },        
};

SR__ s_47[] =
{
    { { REQ_DEF}, {   2} },         
    { {     280}, {  60} }, // COMMA
    { {       0}, { -38} },         
};

SR__ s_48[] =
{
    { { REQ_TOKEN}, {  8} },              
    { {       293}, { 61} }, // block     
    { {       272}, { 15} }, // BEGIN     
    { {       291}, { 16} }, // identifier
    { {       295}, { 17} }, // func_call 
    { {       296}, { 18} }, // if_block  
    { {       270}, { 12} }, // ID        
    { {       274}, { 19} }, // IF        
    { {         0}, {  0} },              
};

SR__ s_49[] =
{
    { { REQ_TOKEN}, { 12} },              
    { {       297}, { 62} }, // ar_expr   
    { {       298}, { 31} }, // summand   
    { {       299}, { 32} }, // factor    
    { {       264}, { 33} }, // MINUS     
    { {       300}, { 34} }, // atom      
    { {       257}, { 35} }, // INT       
    { {       258}, { 36} }, // FLOAT     
    { {       295}, { 37} }, // func_call 
    { {       291}, { 38} }, // identifier
    { {       278}, { 39} }, // LPAR      
    { {       270}, { 12} }, // ID        
    { {         0}, {  0} },              
};

SR__ s_50[] =
{
    { { REQ_TOKEN}, { 12} },              
    { {       297}, { 63} }, // ar_expr   
    { {       298}, { 31} }, // summand   
    { {       299}, { 32} }, // factor    
    { {       264}, { 33} }, // MINUS     
    { {       300}, { 34} }, // atom      
    { {       257}, { 35} }, // INT       
    { {       258}, { 36} }, // FLOAT     
    { {       295}, { 37} }, // func_call 
    { {       291}, { 38} }, // identifier
    { {       278}, { 39} }, // LPAR      
    { {       270}, { 12} }, // ID        
    { {         0}, {  0} },              
};

SR__ s_51[] =
{
    { { REQ_TOKEN}, { 11} },              
    { {       298}, { 64} }, // summand   
    { {       299}, { 32} }, // factor    
    { {       264}, { 33} }, // MINUS     
    { {       300}, { 34} }, // atom      
    { {       257}, { 35} }, // INT       
    { {       258}, { 36} }, // FLOAT     
    { {       295}, { 37} }, // func_call 
    { {       291}, { 38} }, // identifier
    { {       278}, { 39} }, // LPAR      
    { {       270}, { 12} }, // ID        
    { {         0}, {  0} },              
};

SR__ s_52[] =
{
    { { REQ_TOKEN}, { 11} },              
    { {       298}, { 65} }, // summand   
    { {       299}, { 32} }, // factor    
    { {       264}, { 33} }, // MINUS     
    { {       300}, { 34} }, // atom      
    { {       257}, { 35} }, // INT       
    { {       258}, { 36} }, // FLOAT     
    { {       295}, { 37} }, // func_call 
    { {       291}, { 38} }, // identifier
    { {       278}, { 39} }, // LPAR      
    { {       270}, { 12} }, // ID        
    { {         0}, {  0} },              
};

SR__ s_53[] =
{
    { { REQ_TOKEN}, { 10} },              
    { {       299}, { 66} }, // factor    
    { {       264}, { 33} }, // MINUS     
    { {       300}, { 34} }, // atom      
    { {       257}, { 35} }, // INT       
    { {       258}, { 36} }, // FLOAT     
    { {       295}, { 37} }, // func_call 
    { {       291}, { 38} }, // identifier
    { {       278}, { 39} }, // LPAR      
    { {       270}, { 12} }, // ID        
    { {         0}, {  0} },              
};

SR__ s_54[] =
{
    { { REQ_TOKEN}, { 10} },              
    { {       299}, { 67} }, // factor    
    { {       264}, { 33} }, // MINUS     
    { {       300}, { 34} }, // atom      
    { {       257}, { 35} }, // INT       
    { {       258}, { 36} }, // FLOAT     
    { {       295}, { 37} }, // func_call 
    { {       291}, { 38} }, // identifier
    { {       278}, { 39} }, // LPAR      
    { {       270}, { 12} }, // ID        
    { {         0}, {  0} },              
};

SR__ s_55[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -29} }, 
};

SR__ s_56[] =
{
    { { REQ_TOKEN}, {  2} },        
    { {       279}, { 68} }, // RPAR
    { {         0}, {  0} },        
};

SR__ s_57[] =
{
    { { DEF_RED}, {  1} }, 
    { {       0}, { -7} }, 
};

SR__ s_58[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -15} }, 
};

SR__ s_59[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -37} }, 
};

SR__ s_60[] =
{
    { { REQ_TOKEN}, { 14} },              
    { {       301}, { 69} }, // arglist   
    { {       294}, { 47} }, // expr      
    { {       297}, { 30} }, // ar_expr   
    { {       298}, { 31} }, // summand   
    { {       299}, { 32} }, // factor    
    { {       264}, { 33} }, // MINUS     
    { {       300}, { 34} }, // atom      
    { {       257}, { 35} }, // INT       
    { {       258}, { 36} }, // FLOAT     
    { {       295}, { 37} }, // func_call 
    { {       291}, { 38} }, // identifier
    { {       278}, { 39} }, // LPAR      
    { {       270}, { 12} }, // ID        
    { {         0}, {  0} },              
};

SR__ s_61[] =
{
    { { REQ_DEF}, {   2} },        
    { {     276}, {  70} }, // ELSE
    { {       0}, { -18} },        
};

SR__ s_62[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -21} }, 
};

SR__ s_63[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -22} }, 
};

SR__ s_64[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -23} }, 
};

SR__ s_65[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -24} }, 
};

SR__ s_66[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -26} }, 
};

SR__ s_67[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -27} }, 
};

SR__ s_68[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -35} }, 
};

SR__ s_69[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -39} }, 
};

SR__ s_70[] =
{
    { { REQ_TOKEN}, {  8} },              
    { {       293}, { 71} }, // block     
    { {       272}, { 15} }, // BEGIN     
    { {       291}, { 16} }, // identifier
    { {       295}, { 17} }, // func_call 
    { {       296}, { 18} }, // if_block  
    { {       270}, { 12} }, // ID        
    { {       274}, { 19} }, // IF        
    { {         0}, {  0} },              
};

SR__ s_71[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -19} }, 
};


// State array:
SR__ *s_state[] =
{
  s_0,  s_1,  s_2,  s_3,  s_4,  s_5,  s_6,  s_7,  s_8,  s_9,
  s_10,  s_11,  s_12,  s_13,  s_14,  s_15,  s_16,  s_17,  s_18,  s_19,
  s_20,  s_21,  s_22,  s_23,  s_24,  s_25,  s_26,  s_27,  s_28,  s_29,
  s_30,  s_31,  s_32,  s_33,  s_34,  s_35,  s_36,  s_37,  s_38,  s_39,
  s_40,  s_41,  s_42,  s_43,  s_44,  s_45,  s_46,  s_47,  s_48,  s_49,
  s_50,  s_51,  s_52,  s_53,  s_54,  s_55,  s_56,  s_57,  s_58,  s_59,
  s_60,  s_61,  s_62,  s_63,  s_64,  s_65,  s_66,  s_67,  s_68,  s_69,
  s_70,  s_71,
};

} // anonymous namespace ends



// If the parsing function call uses arguments, then provide an overloaded
// function.  The code below doesn't rely on parameters, so no arguments are
// required.  Furthermore, parse uses a function try block to allow us to do
// ACCEPT and ABORT from anywhere, even from within members called by actions,
// simply throwing the appropriate exceptions.

ParserBase::ParserBase()
:
    d_stackIdx__(-1),
    // $insert debuginit
    d_debug__(false),
    d_nErrors__(0),
    // $insert requiredtokens
    d_requiredTokens__(0),
    d_acceptedTokens__(d_requiredTokens__),
    d_token__(_UNDETERMINED_),
    d_nextToken__(_UNDETERMINED_)
{}


void Parser::print__()
{
// $insert print
}

void ParserBase::clearin()
{
    d_token__ = d_nextToken__ = _UNDETERMINED_;
}

void ParserBase::push__(size_t state)
{
    if (static_cast<size_t>(d_stackIdx__ + 1) == d_stateStack__.size())
    {
        size_t newSize = d_stackIdx__ + STACK_EXPANSION;
        d_stateStack__.resize(newSize);
        d_valueStack__.resize(newSize);
    }
    ++d_stackIdx__;
    d_stateStack__[d_stackIdx__] = d_state__ = state;
    *(d_vsp__ = &d_valueStack__[d_stackIdx__]) = d_val__;
}

void ParserBase::popToken__()
{
    d_token__ = d_nextToken__;

    d_val__ = d_nextVal__;
    d_nextVal__ = STYPE__();

    d_nextToken__ = _UNDETERMINED_;
}
     
void ParserBase::pushToken__(int token)
{
    d_nextToken__ = d_token__;
    d_nextVal__ = d_val__;
    d_token__ = token;
}
     
void ParserBase::pop__(size_t count)
{
    if (d_stackIdx__ < static_cast<int>(count))
    {
        ABORT();
    }

    d_stackIdx__ -= count;
    d_state__ = d_stateStack__[d_stackIdx__];
    d_vsp__ = &d_valueStack__[d_stackIdx__];
}

inline size_t ParserBase::top__() const
{
    return d_stateStack__[d_stackIdx__];
}

void Parser::executeAction(int production)
{
    if (d_token__ != _UNDETERMINED_)
        pushToken__(d_token__);     // save an already available token

                                    // save default non-nested block $$
    if (int size = s_productionInfo[production].d_size)
        d_val__ = d_vsp__[1 - size];

    switch (production)
    {
        // $insert actioncases
        
        case 1:
#line 47 "parser.y"
        {
         std::string head("#include<iostream>\n"
         "template <typename T1> void write(T1 a1){std::cout << a1;}\n"
         "template <typename T1, typename T2> void write(T1 a1, T2 a2){std::cout << a1 << a2;}\n"
         "template <typename T1, typename T2, typename T3> void write(T1 a1, T2 a2, T3 a3){std::cout << a1 << a2 << a3;}\n"
         "template <typename T1> void writeln(T1 a1){std::cout << a1 << '\\n';}\n"
         "template <typename T1, typename T2> void writeln(T1 a1, T2 a2){std::cout << a1 << a2 << '\\n';}\n"
         "template <typename T1, typename T2, typename T3> void writeln(T1 a1, T2 a2, T3 a3){std::cout << a1 << a2 << a3 << '\\n';}\n"
         "template <typename T1> void read(T1 &a1){std::cin >> a1;}\n"
         "template <typename T1, typename T2> void read(T1 &a1, T2 &a2){std::cin >> a1 >> a2;}\n"
         "template <typename T1, typename T2, typename T3> void read(T1 &a1, T2 &a2, T3 &a3){std::cin >> a1 >> a2 >> a3;}\n"
         "\n");
         std::string text(d_vsp__[-1] + '\n' +d_vsp__[0]);
         std::cout << head << text;
         }
        break;

        case 3:
#line 69 "parser.y"
        {
         d_val__ = d_vsp__[-1] + '\n' + d_vsp__[0];
         }
        break;

        case 4:
#line 75 "parser.y"
        {
         d_val__ = d_vsp__[0];
         }
        break;

        case 6:
#line 83 "parser.y"
        {
         d_val__ = d_vsp__[-1] + '\n' + d_vsp__[0];
         }
        break;

        case 7:
#line 89 "parser.y"
        {
         std::string type_name = d_vsp__[-1];
         if (type_name == "integer")
         type_name = std::string("int");
         else if (type_name == "real")
         type_name = std::string("double");
         else if (type_name == "boolean")
         type_name = std::string("bool");
         d_val__ = type_name + ' ' + d_vsp__[-3] + ';';
         }
        break;

        case 8:
#line 102 "parser.y"
        {
         d_val__ = d_vsp__[0];
         }
        break;

        case 9:
#line 106 "parser.y"
        {
         d_val__ = d_vsp__[-2] + ", " + d_vsp__[0];
         }
        break;

        case 10:
#line 112 "parser.y"
        {
         d_val__ = d_scanner.matched();
         }
        break;

        case 11:
#line 118 "parser.y"
        {
         d_val__ = std::string("int main()\n{\n") + d_vsp__[-2] + "return 0;\n}";
         }
        break;

        case 13:
#line 125 "parser.y"
        {
         d_val__ = d_vsp__[-1] + "\n" + d_vsp__[0];
         }
        break;

        case 14:
#line 131 "parser.y"
        {
         d_val__ = std::string("{\n") + d_vsp__[-1] + "}";
         }
        break;

        case 15:
#line 135 "parser.y"
        {
         d_val__ = d_vsp__[-3] + " = " + d_vsp__[-1] + ";";
         }
        break;

        case 16:
#line 139 "parser.y"
        {
         d_val__ = d_vsp__[-1] + ";";
         }
        break;

        case 17:
#line 143 "parser.y"
        {
         d_val__ = d_vsp__[0];
         }
        break;

        case 18:
#line 149 "parser.y"
        {
         d_val__ = std::string("if (") + d_vsp__[-2] + ")\n" + d_vsp__[0];
         }
        break;

        case 19:
#line 153 "parser.y"
        {
         d_val__ = std::string("if (") + d_vsp__[-4] + ")\n" + d_vsp__[-2] + "else\n" + d_vsp__[0];
         }
        break;

        case 20:
#line 160 "parser.y"
        {
         d_val__ = d_vsp__[0];
         }
        break;

        case 21:
#line 164 "parser.y"
        {
         d_val__ = d_vsp__[-2] + " == " + d_vsp__[0];
         }
        break;

        case 22:
#line 168 "parser.y"
        {
         d_val__ = d_vsp__[-2] + " != " + d_vsp__[0];
         }
        break;

        case 23:
#line 175 "parser.y"
        {
         d_val__ = d_vsp__[-2] + " + " + d_vsp__[0];
         }
        break;

        case 24:
#line 179 "parser.y"
        {
         d_val__ = d_vsp__[-2] + " - " + d_vsp__[0];
         }
        break;

        case 25:
#line 183 "parser.y"
        {
         d_val__ = d_vsp__[0];
         }
        break;

        case 26:
#line 189 "parser.y"
        {
         d_val__ = d_vsp__[-2] + " * " + d_vsp__[0];
         }
        break;

        case 27:
#line 193 "parser.y"
        {
         d_val__ = d_vsp__[-2] + " / " + d_vsp__[0];
         }
        break;

        case 28:
#line 197 "parser.y"
        {
         d_val__ = d_vsp__[0];
         }
        break;

        case 29:
#line 203 "parser.y"
        {
         d_val__ = std::string("- ") + d_vsp__[0];
         }
        break;

        case 30:
#line 207 "parser.y"
        {
         d_val__ = d_vsp__[0];
         }
        break;

        case 31:
#line 213 "parser.y"
        {
         d_val__ = d_scanner.matched().c_str();
         }
        break;

        case 32:
#line 217 "parser.y"
        {
         d_val__ = d_scanner.matched().c_str();
         }
        break;

        case 33:
#line 221 "parser.y"
        {
         d_val__ = d_vsp__[0];
         }
        break;

        case 34:
#line 225 "parser.y"
        {
         d_val__ = d_vsp__[0];
         }
        break;

        case 35:
#line 229 "parser.y"
        {
         d_val__ = std::string("(") + d_vsp__[-1] + ")";
         }
        break;

        case 36:
#line 235 "parser.y"
        {
         d_val__ = d_vsp__[-2];
         }
        break;

        case 37:
#line 239 "parser.y"
        {
         if (d_vsp__[-3] == "print")
         {
         d_val__ = std::string("$print$") + "(" + d_vsp__[-1] + ")";
         }
         if (d_vsp__[-3] == "println")
         {
         }
         if (d_vsp__[-3] == "write")
         {
         }
         if (d_vsp__[-3] == "writeln")
         {
         }
         d_val__ = d_vsp__[-3] + "(" + d_vsp__[-1] + ")";
         }
        break;

        case 38:
#line 258 "parser.y"
        {
         d_val__ = d_vsp__[0];
         }
        break;

        case 39:
#line 262 "parser.y"
        {
         d_val__ = d_vsp__[-2] + ", " + d_vsp__[0];
         }
        break;

    }
}

inline void ParserBase::reduce__(PI__ const &pi)
{
    d_token__ = pi.d_nonTerm;
    pop__(pi.d_size);

}

// If d_token__ is _UNDETERMINED_ then if d_nextToken__ is _UNDETERMINED_ another
// token is obtained from lex(). Then d_nextToken__ is assigned to d_token__.
void Parser::nextToken()
{
    if (d_token__ != _UNDETERMINED_)        // no need for a token: got one
        return;                             // already

    if (d_nextToken__ != _UNDETERMINED_)
    {
        popToken__();                       // consume pending token
    }
    else
    {
        ++d_acceptedTokens__;               // accept another token (see
                                            // errorRecover())
        d_token__ = lex();
        if (d_token__ <= 0)
            d_token__ = _EOF_;
    }
    print();
}

// if the final transition is negative, then we should reduce by the rule
// given by its positive value. Note that the `recovery' parameter is only
// used with the --debug option
int Parser::lookup(bool recovery)
{
    // $insert threading
    SR__ *sr = s_state[d_state__];        // get the appropriate state-table
    int lastIdx = sr->d_lastIdx;        // sentinel-index in the SR__ array

    SR__ *lastElementPtr = sr + lastIdx;
    SR__ *elementPtr = sr + 1;            // start the search at s_xx[1]

    lastElementPtr->d_token = d_token__;// set search-token

    while (elementPtr->d_token != d_token__)
        ++elementPtr;

    if (elementPtr == lastElementPtr)   // reached the last element
    {
        if (elementPtr->d_action < 0)   // default reduction
        {
            return elementPtr->d_action;                
        }

        // No default reduction, so token not found, so error.
        throw UNEXPECTED_TOKEN__;
    }

    // not at the last element: inspect the nature of the action
    // (< 0: reduce, 0: ACCEPT, > 0: shift)

    int action = elementPtr->d_action;


    return action;
}

    // When an error has occurred, pop elements off the stack until the top
    // state has an error-item. If none is found, the default recovery
    // mode (which is to abort) is activated. 
    //
    // If EOF is encountered without being appropriate for the current state,
    // then the error recovery will fall back to the default recovery mode.
    // (i.e., parsing terminates)
void Parser::errorRecovery()
try
{
    if (d_acceptedTokens__ >= d_requiredTokens__)// only generate an error-
    {                                           // message if enough tokens 
        ++d_nErrors__;                          // were accepted. Otherwise
        error("Syntax error");                  // simply skip input

    }


    // get the error state
    while (not (s_state[top__()][0].d_type & ERR_ITEM))
    {
        pop__();
    }

    // In the error state, lookup a token allowing us to proceed.
    // Continuation may be possible following multiple reductions,
    // but eventuall a shift will be used, requiring the retrieval of
    // a terminal token. If a retrieved token doesn't match, the catch below 
    // will ensure the next token is requested in the while(true) block
    // implemented below:

    int lastToken = d_token__;                  // give the unexpected token a
                                                // chance to be processed
                                                // again.

    pushToken__(_error_);                       // specify _error_ as next token
    push__(lookup(true));                       // push the error state

    d_token__ = lastToken;                      // reactivate the unexpected
                                                // token (we're now in an
                                                // ERROR state).

    bool gotToken = true;                       // the next token is a terminal

    while (true)
    {
        try
        {
            if (s_state[d_state__]->d_type & REQ_TOKEN)
            {
                gotToken = d_token__ == _UNDETERMINED_;
                nextToken();                    // obtain next token
            }
            
            int action = lookup(true);

            if (action > 0)                 // push a new state
            {
                push__(action);
                popToken__();

                if (gotToken)
                {

                    d_acceptedTokens__ = 0;
                    return;
                }
            }
            else if (action < 0)
            {
                // no actions executed on recovery but save an already 
                // available token:
                if (d_token__ != _UNDETERMINED_)
                    pushToken__(d_token__);
 
                                            // next token is the rule's LHS
                reduce__(s_productionInfo[-action]); 
            }
            else
                ABORT();                    // abort when accepting during
                                            // error recovery
        }
        catch (...)
        {
            if (d_token__ == _EOF_)
                ABORT();                    // saw inappropriate _EOF_
                      
            popToken__();                   // failing token now skipped
        }
    }
}
catch (ErrorRecovery__)       // This is: DEFAULT_RECOVERY_MODE
{
    ABORT();
}

    // The parsing algorithm:
    // Initially, state 0 is pushed on the stack, and d_token__ as well as
    // d_nextToken__ are initialized to _UNDETERMINED_. 
    //
    // Then, in an eternal loop:
    //
    //  1. If a state does not have REQ_TOKEN no token is assigned to
    //     d_token__. If the state has REQ_TOKEN, nextToken() is called to
    //      determine d_nextToken__ and d_token__ is set to
    //     d_nextToken__. nextToken() will not call lex() unless d_nextToken__ is 
    //     _UNDETERMINED_. 
    //
    //  2. lookup() is called: 
    //     d_token__ is stored in the final element's d_token field of the
    //     state's SR_ array. 
    //
    //  3. The current token is looked up in the state's SR_ array
    //
    //  4. Depending on the result of the lookup() function the next state is
    //     shifted on the parser's stack, a reduction by some rule is applied,
    //     or the parsing function returns ACCEPT(). When a reduction is
    //     called for, any action that may have been defined for that
    //     reduction is executed.
    //
    //  5. An error occurs if d_token__ is not found, and the state has no
    //     default reduction. Error handling was described at the top of this
    //     file.

int Parser::parse()
try 
{
    push__(0);                              // initial state
    clearin();                              // clear the tokens.

    while (true)
    {
        try
        {
            if (s_state[d_state__]->d_type & REQ_TOKEN)
                nextToken();                // obtain next token


            int action = lookup(false);     // lookup d_token__ in d_state__

            if (action > 0)                 // SHIFT: push a new state
            {
                push__(action);
                popToken__();               // token processed
            }
            else if (action < 0)            // REDUCE: execute and pop.
            {
                executeAction(-action);
                                            // next token is the rule's LHS
                reduce__(s_productionInfo[-action]); 
            }
            else 
                ACCEPT();
        }
        catch (ErrorRecovery__)
        {
            errorRecovery();
        }
    }
}
catch (Return__ retValue)
{
    return retValue;
}




