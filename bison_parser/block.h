#ifndef BLOCK_H
#define BLOCK_H

#include "node.h"

namespace tokens
{

class Block : public Node
{
public:
    Block();
    std::string to_string() const {return std::string();};
};

}

#endif // BLOCK_H
