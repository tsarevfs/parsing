%scanner Scanner.h

%baseclass-preinclude parser_include.h

%token INT
%token FLOAT

%token FUNC
%token PROC

%token AND
%token OR
%token PLUS
%token MINUS
%token MUL
%token DIV

%token ASSIGN
%token EQ
%token NEQ

%token ID

%token VAR
%token BEGIN
%token END
%token IF
%token THEN
%token ELSE

%token CONST
%token LPAR
%token RPAR

%token COMMA
%token COLON
%token SEMICOL
%token DOT

%stype std::string

%start program
%%

program:
    var_sections body
    {
        std::string head("#include<iostream>\n"
                         "template <typename T1> void write(T1 a1){std::cout << a1;}\n"
                         "template <typename T1, typename T2> void write(T1 a1, T2 a2){std::cout << a1 << a2;}\n"
                         "template <typename T1, typename T2, typename T3> void write(T1 a1, T2 a2, T3 a3){std::cout << a1 << a2 << a3;}\n"

                         "template <typename T1> void writeln(T1 a1){std::cout << a1 << '\\n';}\n"
                         "template <typename T1, typename T2> void writeln(T1 a1, T2 a2){std::cout << a1 << a2 << '\\n';}\n"
                         "template <typename T1, typename T2, typename T3> void writeln(T1 a1, T2 a2, T3 a3){std::cout << a1 << a2 << a3 << '\\n';}\n"

                         "template <typename T1> void read(T1 &a1){std::cin >> a1;}\n"
                         "template <typename T1, typename T2> void read(T1 &a1, T2 &a2){std::cin >> a1 >> a2;}\n"
                         "template <typename T1, typename T2, typename T3> void read(T1 &a1, T2 &a2, T3 &a3){std::cin >> a1 >> a2 >> a3;}\n"
                         "\n");
        std::string text($1 + '\n' +$2);

        std::cout << head << text;
    };

var_sections:
    //none
    | var_section var_sections
    {
        $$ = $1 + '\n' + $2;
    };

var_section:
    VAR declarations
    {
        $$ = $2;
    };


declarations:
    //none
    | declaration declarations
    {
        $$ = $1 + '\n' + $2;
    };

declaration:
    identifiers COLON identifier SEMICOL
    {
        std::string type_name = $3;
        if (type_name == "integer")
            type_name = std::string("int");
        else if (type_name == "real")
            type_name = std::string("double");
        else if (type_name == "boolean")
            type_name = std::string("bool");
        $$ = type_name + ' ' + $1 + ';';
    };

identifiers:
    identifier
    {
        $$ = $1;
    }
    | identifier COMMA identifiers
    {
        $$ = $1 + ", " + $3;
    };

identifier:
    ID
    {
        $$ = d_scanner.matched();
    };

body:
    BEGIN blocks END DOT
    {
        $$ = std::string("int main()\n{\n") + $2 + "return 0;\n}";
    };

blocks:
    //none
    | block blocks
    {
        $$ = $1 + "\n" + $2;
    };

block:
    BEGIN blocks END
    {
        $$ = std::string("{\n") + $2 + "}";
    }
    |identifier ASSIGN expr SEMICOL
    {
        $$ = $1 + " = " + $3 + ";";
    }
    | func_call SEMICOL
    {
        $$ = $1 + ";";
    }
    | if_block
    {
        $$ = $1;
    };

if_block:
    IF expr THEN block
    {
        $$ = std::string("if (") + $2 + ")\n" + $4;
    }
    | IF expr THEN block ELSE block
    {
        $$ = std::string("if (") + $2 + ")\n" + $4 + "else\n" + $6;
    };


expr:
     ar_expr
     {
         $$ = $1;
     }
     | ar_expr EQ ar_expr
     {
        $$ = $1 + " == " + $3;
     }
     | ar_expr NEQ ar_expr
     {
        $$ = $1 + " != " + $3;
     };


ar_expr:
    summand PLUS summand
    {
        $$ = $1 + " + " + $3;
    }
    | summand MINUS summand
    {
        $$ = $1 + " - " + $3;
    }
    | summand
    {
        $$ = $1;
    };

summand:
    factor MUL factor
    {
        $$ = $1 + " * " + $3;
    }
    | factor DIV factor
    {
        $$ = $1 + " / " + $3;
    }
    | factor
    {
        $$ = $1;
    };

factor:
    MINUS atom
    {
        $$ = std::string("- ") + $2;
    }
    | atom
    {
        $$ = $1;
    };

atom:
    INT
    {
        $$ = d_scanner.matched().c_str();
    }
    | FLOAT
    {
        $$ = d_scanner.matched().c_str();
    }
    | func_call
    {
        $$ = $1;
    }
    | identifier
    {
        $$ = $1;
    }
    | LPAR expr RPAR
    {
        $$ = std::string("(") + $2 + ")";
    };

func_call:
    identifier LPAR RPAR
    {
        $$ = $1;
    }
    | identifier LPAR arglist RPAR
    {
        if ($1 == "print")
        {
            $$ = std::string("$print$") + "(" + $3 + ")";
        }
        if ($1 == "println")
        {
        }
        if ($1 == "write")
        {
        }
        if ($1 == "writeln")
        {
        }
        $$ = $1 + "(" + $3 + ")";
    };

arglist:
    expr
    {
        $$ = $1;
    }
    | expr COMMA arglist
    {
        $$ = $1 + ", " + $3;
    };
