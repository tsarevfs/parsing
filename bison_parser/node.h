#ifndef NODE_H
#define NODE_H

#include <string>

namespace tokens
{

class Node
{
public:
    virtual std::string to_string() const = 0;
    Node();
};

}

#endif // NODE_H
