#ifndef KEYWORDTOKEN_H
#define KEYWORDTOKEN_H

#include "token.h"

namespace tokens
{

enum KEYWORDS
{
    BEGIN,
    END,
    IF,
    THEN,
    VAR,
    NONE //invalid value
};

class KeyWordToken : public Token
{
public:
    KeyWordToken();
    KeyWordToken(KEYWORDS value);
    std::string to_string() const;
private:
    KEYWORDS value;
};

}

#endif // KEYWORDTOKEN_H
