TEMPLATE = app
CONFIG += console
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++0x


SOURCES += \
    parse.cc \
    main.cc \
    lex.cc

OTHER_FILES += \
    scanner.l \
    parser.y

HEADERS += \
    Scannerbase.h \
    Scanner.ih \
    Scanner.h \
    Parserbase.h \
    Parser.ih \
    Parser.h \
    parser_include.h

