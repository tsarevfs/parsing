#ifndef TOKEN_H
#define TOKEN_H

#include <iostream>
#include <string>
#include "node.h"

namespace tokens
{

class Token : public Node
{
public:
    Token();
    std::string to_string() const;
};

}

#endif // TOKEN_H
