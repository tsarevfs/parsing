#!/usr/bin/python

from lexer_ import Lexer
from parser_ import Parser
import sys

def main():
	try:
		lex = Lexer(sys.argv[1])
		pars = Parser(lex)
		print(pars.parse())
	except RuntimeError as e:
		print(str(e))
		
if __name__ == '__main__':
	main()