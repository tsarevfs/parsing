#!/usr/bin/python3

import re
import sys

class WrongTokenDescription(RuntimeError):
	pass

def gen(fin):
	token_re = re.compile(r'(_|[A-Z]*)\s*\"(.+)\"')
	token_descrs = []

	for n, s in enumerate(fin.readlines()):
		token = token_re.match(s)
		if token:
			token_descrs.append(token.groups())
		else:
			raise WrongTokenDescription('Fails to read %d-nd rule' % (n + 1))

	fin.close()
	lexer_text = "\
import re\n\
\n\
class LexerError(RuntimeError):\n\
	pass\n\
\n\
class Token(object):\n\
	def __init__(self, name, str_value):\n\
		self.name = name\n\
		self.str_value = str_value\n\
\n\
class Lexer(object):\n\
	def __init__(self, input):\n\
		self._input = input\n\
		token_descrs = %s\n\
		self._token_descrs = [(name, re.compile(r)) for (name, r) in token_descrs]\n\
		self._tokens = [name for (name, _) in token_descrs]\n\
		self._pos = 0\n\
\n\
	def next_token(self):\n\
		if self._pos == len(self._input):\n\
			return\n\
		while True:\n\
			matched = False\n\
			for token_name, token_re in self._token_descrs:\n\
				m = token_re.match(self._input[self._pos])\n\
				if m:\n\
					matched = True\n\
					if token_name == '_':\n\
						self._pos += m.end()\n\
						break\n\
					return Token(token_name, m.group())\n\
			if not matched:\n\
				raise LexerError(\'Cant match token at pos \' + str(self._pos))\n\
	def pop(self):\n\
		while True:\n\
			for token_name, token_re in self._token_descrs:\n\
				m = token_re.match(self._input[self._pos])\n\
				if m:\n\
					self._pos += m.end()\n\
					if token_name == '_':\n\
						break\n\
					return\n\
\n\
	def has_next(self):\n\
		return self._pos != len(self._input)\n\
\n\
def main():\n\
	print(\'don\\\'t run this file\')\n\
\n\
if __name__ == '__main__':\n\
	main()\n\
	"
	return lexer_text % str(token_descrs)


def main():
	fin_name = 'lexer.txt'
	fout_name = 'lexer_.py'
	if len(sys.argv) == 3:
		fin_name = sys.argv[1]
		fout_name = sys.argv[2]
	fin = open(fin_name, 'r')
	fout = open(fout_name, 'w')
	fout.write(gen(fin))
	fout.close()

if __name__ == '__main__':
	main()