from node import Node
class ParsingError(RuntimeError):
	pass

class Parser(object):
	def __init__(self, lexer):
		self._lexer = lexer
		self._calc_funcs = {'expr' : [self.expr_1], 'r' : [self.r_2, self.r_3], 'num' : [self.num_4], 'eps' : [self.eps_6], 'op' : [self.op_5]}
		self._FIRST = {'num': {'NUM'}, 'expr': {'NUM'}, 'OP': {'OP'}, 'r': {'NUM', '$eps'}, 'NUM': {'NUM'}, 'eps': {'$eps'}, 'op': {'OP'}}
		self._rules = {'expr': [['num', 'r']], 'r': [['expr', 'op', 'r'], ['eps']], 'num': [['NUM']], 'eps': [[]], 'op': [['OP']]}
		self._terms = ['NUM', 'OP']
		self._start_token = 'expr'

	def _first(self, right_elems):
		first = set()
		produce_eps = True
		for X in right_elems:
			first |= self._FIRST[X] - {'$eps'}
			if not '$eps' in self._FIRST[X]:
				produce_eps = False
				break;
		if produce_eps:
			first.add('$eps')

		return first

	def parse(self):
		return self.parse_(self._start_token)

	def parse_(self, unterm):
		args = []
		ok = False
		nrule = None
		cur_token = None
		if self._lexer.has_next():
			cur_token = self._lexer.next_token()
			for n, rule in enumerate(self._rules[unterm]):
				if cur_token.name in self._first(rule):
					ok = True
					nrule = n
					for elem in rule:
						if elem in self._terms:
							if not cur_token:
								raise ParsingError('Unexpected end')
							if cur_token.name != elem:
								raise ParsingError('Unexpected token ' + cur_token.name + ', ' + ' expected ' + elem)
							args.append(cur_token.str_value)
							self._lexer.pop()
							cur_token = self._lexer.next_token()
						else:
							args.append(self.parse_(elem))
							cur_token = self._lexer.next_token()
					break

		if not ok:
			for n, rule in enumerate(self._rules[unterm]):
				if '$eps' in self._first(rule):
					ok = True
					nrule = n
					for elem in rule:
						args.append(self.parse_(elem))

		if not ok:
			if cur_token:
				raise ParsingError('Cant choose rule to parse ' + unterm + ' stated with ' + cur_token.name)
			else:
				raise ParsingError('Expected some tokens to parse ' + unterm + ' but end of tokens founded')
		return self._calc_funcs[unterm][nrule](*args)
	def expr_1(self, num, r):
		funcs = r[:]
		value = num
		while len(funcs) > 0:
			func = funcs.pop()
			value = func(value)
		return value

	def r_2(self, expr, op, r1):
		funcs = r1[:]
		if op == '+':
			funcs.append(lambda x : expr + x)
			return funcs
		elif op == '*':
			funcs.append(lambda x : expr * x)
			return funcs

	def r_3(self, eps):
		return []

	def num_4(self, num):
		return int(num)

	def op_5(self, op):
		return op

	def eps_6(self, ):
		return None

