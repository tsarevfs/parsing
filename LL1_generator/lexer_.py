import re

class LexerError(RuntimeError):
	pass

class Token(object):
	def __init__(self, name, str_value):
		self.name = name
		self.str_value = str_value

class Lexer(object):
	def __init__(self, input):
		self._input = input
		token_descrs = [('INTEGER', '[0-9]+'), ('LPAREN', '\\('), ('RPAREN', '\\)'), ('PLUS', '\\+'), ('MUL', '\\*'), ('_', '[\\s\\t\\r]')]
		self._token_descrs = [(name, re.compile(r)) for (name, r) in token_descrs]
		self._tokens = [name for (name, _) in token_descrs]
		self._pos = 0

	def next_token(self):
		if self._pos == len(self._input):
			return
		while True:
			matched = False
			for token_name, token_re in self._token_descrs:
				m = token_re.match(self._input[self._pos])
				if m:
					matched = True
					if token_name == '_':
						self._pos += m.end()
						break
					return Token(token_name, m.group())
			if not matched:
				raise LexerError('Cant match token at pos ' + str(self._pos))
	def pop(self):
		while True:
			for token_name, token_re in self._token_descrs:
				m = token_re.match(self._input[self._pos])
				if m:
					self._pos += m.end()
					if token_name == '_':
						break
					return

	def has_next(self):
		return self._pos != len(self._input)

def main():
	print('don\'t run this file')

if __name__ == '__main__':
	main()
	