class ParsingError(RuntimeError):
	pass

class Parser(object):
	def __init__(self, lexer):
		self._lexer = lexer
		self._calc_funcs = {'ePrime' : [self.ePrime_3, self.ePrime_4], 'e' : [self.e_2], 'f' : [self.f_8, self.f_9], 'tPrime' : [self.tPrime_6, self.tPrime_7], 's' : [self.s_1], 't' : [self.t_5]}
		self._FIRST = {'ePrime': {'PLUS', '$eps'}, 'RPAREN': {'RPAREN'}, 'tPrime': {'MUL', '$eps'}, 'f': {'INTEGER', 'LPAREN'}, 's': {'INTEGER', 'LPAREN'}, 'PLUS': {'PLUS'}, 't': {'INTEGER', 'LPAREN'}, 'LPAREN': {'LPAREN'}, 'INTEGER': {'INTEGER'}, 'MUL': {'MUL'}, 'e': {'INTEGER', 'LPAREN'}}
		self._rules = {'ePrime': [['PLUS', 't', 'ePrime'], []], 's': [['e']], 'e': [['t', 'ePrime']], 't': [['f', 'tPrime']], 'f': [['INTEGER'], ['LPAREN', 'e', 'RPAREN']], 'tPrime': [['MUL', 'f', 'tPrime'], []]}
		self._terms = ['INTEGER', 'LPAREN', 'RPAREN', 'PLUS', 'MUL']
		self._start_token = 's'

	def _first(self, right_elems):
		first = set()
		produce_eps = True
		for X in right_elems:
			first |= self._FIRST[X] - {'$eps'}
			if not '$eps' in self._FIRST[X]:
				produce_eps = False
				break;
		if produce_eps:
			first.add('$eps')

		return first

	def parse(self):
		return self.parse_(self._start_token)

	def parse_(self, unterm):
		args = []
		ok = False
		nrule = None
		cur_token = None
		if self._lexer.has_next():
			cur_token = self._lexer.next_token()
			for n, rule in enumerate(self._rules[unterm]):
				if cur_token.name in self._first(rule):
					ok = True
					nrule = n
					for elem in rule:
						if elem in self._terms:
							if not cur_token:
								raise ParsingError('Unexpected end')
							if cur_token.name != elem:
								raise ParsingError('Unexpected token ' + cur_token.name + ', ' + ' expected ' + elem)
							args.append(cur_token.str_value)
							self._lexer.pop()
							cur_token = self._lexer.next_token()
						else:
							args.append(self.parse_(elem))
							cur_token = self._lexer.next_token()
					break

		if not ok:
			for n, rule in enumerate(self._rules[unterm]):
				if '$eps' in self._first(rule):
					ok = True
					nrule = n

		if not ok:
			if cur_token:
				raise ParsingError('Cant choose rule to parse ' + unterm + ' stated with ' + cur_token.name)
			else:
				raise ParsingError('Expected some tokens to parse ' + unterm + ' but end of tokens founded')
		return self._calc_funcs[unterm][nrule](*args)
	def s_1(self, val):
		return val

	def e_2(self, val1, val2):
		return val1 + val2

	def ePrime_3(self, _, val1, val2):
		return val1 + val2

	def ePrime_4(self, ):
		return 0

	def t_5(self, val1, val2):
		return val1 * val2

	def tPrime_6(self, _, val1, val2):
		return val1 * val2

	def tPrime_7(self, ):
		return 1

	def f_8(self, val1):
		return int(val1)

	def f_9(self, _1, e, _2):
		return e


