import pygraphviz as pgv

def gen_id():
	if not hasattr(gen_id, '_id'):
		gen_id._id = 0
	else:
		gen_id._id += 1

	return gen_id._id

class Node(object):
	def __init__(self, value, *children):
		self._children = children
		self._value = value
		self._id = gen_id()

	def __repr__(self):
		return 'Node(%s)' % str(self._children)

	def __str__(self):
		return self.__repr__()

	def draw_graph(self, filename):
		self.G = pgv.AGraph()
		self.fill_graph(None, self.G)
		self.G.layout(prog='dot')
		self.G.draw(filename)

	def fill_graph(self, parent, graph):
		graph.add_node(self._id, label=str(self._value))
		if parent:
			graph.add_edge(parent, self._id)
		
		for child in self._children:
			child.fill_graph(self._id, graph)
			
def main():
	pass

if __name__ == '__main__':
	main()