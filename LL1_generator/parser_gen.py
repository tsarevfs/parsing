#!/usr/bin/python3

import re
from collections import defaultdict
import sys

def gen_id():
	if not hasattr(gen_id, '_id'):
		gen_id._id = 0
	gen_id._id += 1
	return gen_id._id

def gen_unic_name(name):
	return name + ('_%d' % gen_id())

def funclist_to_str(func_list):
	return '[%s]' % ', '.join(['self.' + func for func in func_list])
def funcmap_to_str(func_map):
	return '{%s}' % ', '.join(['\'%s\' : %s' % (token, funclist_to_str(funcs)) for (token, funcs) in func_map.items()])


def gen(fin):
	calc_funcs = defaultdict(list)
	terms = []
	rules = defaultdict(list)
	calc_funcs_text = ''

	rule_left_re = re.compile(r'\$(\w+)\s*:')
	func_title_re = re.compile(r'(def\s)\s*([a-zA-Z_]\w*)\s*(\()(.*)')
	
	start_token = None
	func_lines = None
	func_name = None

	text = ''

	for s in fin.readlines():
		if s.strip() == '':
			continue

		if s.startswith('$import '):
			text += s[1:]
			continue

		if s.startswith('$from '):
			text += s[1:]
			continue

		if s.startswith('$start '):
			start_token = s[7:].strip()
			continue

		if s.startswith('$terms '):
			terms_iter = filter((lambda s: s.strip()), s[7:].split(' '))
			terms = [term.strip() for term in terms_iter]
			continue

		m = rule_left_re.match(s)
		if m:
			if func_name:
				calc_funcs[left_unterm].append(func_name)
				calc_funcs_text += ''.join(func_lines) + '\n'

			left_unterm = None
			right_elems = None
			func_name = None

			left_unterm = m.groups()[0]
			right_elems_iter = filter((lambda s: s.strip()), s[m.end():].split(' '))
			right_elems = [token.strip() for token in right_elems_iter]
			rules[left_unterm].append(right_elems)
			func_lines = []
		else:
			m = func_title_re.match(s)
			if m:
				func_name = gen_unic_name(m.groups()[1])
				s = func_title_re.sub(r'\1%s(self, \4' % func_name, s)
			func_lines.append('	' + s) # +1 tab at begining

	if func_name:
		calc_funcs[left_unterm].append(func_name)
		calc_funcs_text += ''.join(func_lines) + '\n'

	FIRST = defaultdict(set)
#FIRST construction
	for term in terms:
		FIRST[term].add(term)

	for unterm in rules:
		if [] in rules[unterm]:
			FIRST[unterm].add('$eps')

	chainges = True
	first_len = sum([len(FIRST[elem]) for elem in FIRST])
	while chainges:
		for unterm in rules:
			for rule in rules[unterm]:
				produce_eps = True
				for Y in rule:
					FIRST[unterm] |= (FIRST[Y] - {'$eps'})
					if not '$eps' in FIRST[Y]:
						produce_eps = False
						break
				if produce_eps:
					FIRST[unterm].add('$eps')

		new_len = sum([len(FIRST[elem]) for elem in FIRST])
		chainges = first_len != new_len
		first_len = new_len


	parser_template = '\
class ParsingError(RuntimeError):\n\
	pass\n\
\n\
class Parser(object):\n\
	def __init__(self, lexer):\n\
		self._lexer = lexer\n\
		self._calc_funcs = %s\n\
		self._FIRST = %s\n\
		self._rules = %s\n\
		self._terms = %s\n\
		self._start_token = \'%s\'\n\
\n\
	def _first(self, right_elems):\n\
		first = set()\n\
		produce_eps = True\n\
		for X in right_elems:\n\
			first |= self._FIRST[X] - {\'$eps\'}\n\
			if not \'$eps\' in self._FIRST[X]:\n\
				produce_eps = False\n\
				break;\n\
		if produce_eps:\n\
			first.add(\'$eps\')\n\
\n\
		return first\n\
\n\
	def parse(self):\n\
		return self.parse_(self._start_token)\n\
\n\
	def parse_(self, unterm):\n\
		args = []\n\
		ok = False\n\
		nrule = None\n\
		cur_token = None\n\
		if self._lexer.has_next():\n\
			cur_token = self._lexer.next_token()\n\
			for n, rule in enumerate(self._rules[unterm]):\n\
				if cur_token.name in self._first(rule):\n\
					ok = True\n\
					nrule = n\n\
					for elem in rule:\n\
						if elem in self._terms:\n\
							if not cur_token:\n\
								raise ParsingError(\'Unexpected end\')\n\
							if cur_token.name != elem:\n\
								raise ParsingError(\'Unexpected token \' + cur_token.name + \', \' + \' expected \' + elem)\n\
							args.append(cur_token.str_value)\n\
							self._lexer.pop()\n\
							cur_token = self._lexer.next_token()\n\
						else:\n\
							args.append(self.parse_(elem))\n\
							cur_token = self._lexer.next_token()\n\
					break\n\
\n\
		if not ok:\n\
			for n, rule in enumerate(self._rules[unterm]):\n\
				if \'$eps\' in self._first(rule):\n\
					ok = True\n\
					nrule = n\n\
					for elem in rule:\n\
						args.append(self.parse_(elem))\n\
\n\
		if not ok:\n\
			if cur_token:\n\
				raise ParsingError(\'Cant choose rule to parse \' + unterm + \' stated with \' + cur_token.name)\n\
			else:\n\
				raise ParsingError(\'Expected some tokens to parse \' + unterm + \' but end of tokens founded\')\n\
		return self._calc_funcs[unterm][nrule](*args)\n\
%s\
\n\
'
	parser_text = parser_template % (
		funcmap_to_str(calc_funcs)
		, str(dict(FIRST))
		, str(dict(rules))
		, str(terms)
		, start_token
		, calc_funcs_text)

	text += parser_text
	return text

def main():
	fin_name = 'parser.txt'
	fout_name = 'parser_.py'
	if len(sys.argv) == 3:
		fin_name = sys.argv[1]
		fout_name = sys.argv[2]
	fin = open(fin_name, 'r')
	fout = open(fout_name, 'w')

	fout.write(gen(fin))
	fin.close()
	fout.close()

if __name__ == '__main__':
	main()