#!/usr/bin/python

from lexer_postfix_ import Lexer
from parser_postfix_ import Parser
import sys

def main():
	try:
		lex = Lexer(sys.argv[1])
		pars = Parser(lex)
		g = pars.parse()
		g.draw_graph('graph.png')

	except RuntimeError as e:
		print(str(e))
		
if __name__ == '__main__':
	main()